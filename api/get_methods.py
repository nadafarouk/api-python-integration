""" Age Of Empires II API integration
This script allows the app to integrate with v1 of https://age-of-empires-2-api.herokuapp.com
and the api url is stored in api_url

This script requires that requests be installed within the Python
environment you are running this script in.

This file should be imported as a module and contains the following
functions:

    * get_by_unit_name returns dict of unit if exists
"""
import requests

api_url='https://age-of-empires-2-api.herokuapp.com/api/v1/'

def get_unit(unit):

    ''' Gets unit by it's name or id

        Args:
        unit : str or int

        Returns:
            response : dict
            the response if unit was found

    '''

    unit_url=api_url+'unit/'+unit
    response= requests.get(unit_url)
    if response.status_code !=200:
        return {}
    return response.json()

