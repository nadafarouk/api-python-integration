'''
This script tests api/get_methods module
'''
import unittest
from api.get_methods import get_unit
class TestGetByName(unittest.TestCase):

    def test_wrong_unit_name(self):
        '''
        tests api get method against unit name that doesn't exist
        '''
        temp=get_unit("unit-doesn't-exist")
        self.assertEqual(len(temp), 0)

    def test_right_unit_name(self):
        '''
        tests api get method against unit name that exist already
        '''
        temp=get_unit("King")
        self.assertNotEqual(len(temp),0)

    def test_wrong_unit_id(self):
        '''
        tests api get method against unit id that exist already
        '''
        temp=get_unit("11789766536")
        self.assertEqual(len(temp), 0)

    def test_right_unit_id(self):
        '''
        tests api get method against unit id that exist already
        '''
        temp=get_unit("12")
        self.assertNotEqual(len(temp),0)


if __name__ == '__main__':
    unittest.main()