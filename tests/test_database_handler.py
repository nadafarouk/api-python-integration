'''
This script tests DatabaseHandler class
'''
import unittest
from mysql.connector.connection_cext import CMySQLConnection
from db.database_handler import DatabaseHandler
class TestDatabaseHandler(unittest.TestCase):

    def setUp(self):
        self.database_object= DatabaseHandler()

    def test_connection_success(self):
        self.assertIsInstance(self.database_object.db_connection,CMySQLConnection)

    def test_table_exists_for_wrong_table(self):
        '''tests table exists method for a table that doesn't exist
        '''
        self.assertFalse(self.database_object.table_exists("no_such_table"))

    def test_table_exists_for_right_table(self):
        '''
        tests table exists method for a table that exists
        '''
        self.assertTrue(self.database_object.table_exists("db_unit"))

    def test_table_creation(self):
        '''
        tests table creation method
        '''
        with self.assertRaises(Exception):
            self.database_object.create_table()

    def test_find_unit_with_numbers(self):
        '''
        test find unit with integer input
        '''
        temp =self.database_object.find_unit(3)
        self.assertEqual(len(temp),0)

    def test_find_unit_with_characters(self):
        '''
        test find unit with string input
        '''
        temp =self.database_object.find_unit("King")
        self.assertNotEqual(len(temp),0)

    def test_insert_unit_without_dict(self):
        '''
        test insert unit with params other than dict
        '''
        with self.assertRaises(Exception):
            self.database_object.insert_unit("King")

    def test_insert_unit_with_dict(self):
        '''
        test insert unit with dict
        '''
        self.database_object.insert_unit({"id":3,"name":"test","other":"other-value"})
        self.assertEqual(1,1)

if __name__ == '__main__':
    unittest.main()