This is a simple command line python program

- the program is an integration with https://age-of-empires-2-api.herokuapp.com
    user can type the unit name to get unit data
    program stores data retrieved from the api calls into mysql database
    to make the program faster next time user requests the same unit

- program notes
    the program was developed using python 3.6 
    and the packages needed was listed in package_list.txt
    
- usage example
    1. clone the program 
        git clone git@bitbucket.org:nadafarouk/api-python-integration.git
    
    2. create virtualenv inside api-python-integration directory    
    
    3. create database 
        database_name
    
    4. create .env file and use the following template with your own settings
        HOST=
        DATABASE=database_name
        DB_USER=
        DB_PASSWORD=
    
    5. activate virtualenv and install packages used 
        source environment_name/bin/activate
        pip3 install -r package_list.txt
        
    6. run the program 
        python3 -m user.run
        
    7. run the unittests 
        python3 -m unittest    
    
        
    

