""" Age of Empires || main script

This script prints to the user the age of empire || unit details
after the user enters its name as input

The script terminates if
    - couldn't establish database connection probably something wrong with .env
    - couldn't find table and unable to create it

User can use exit word to terminate the script
    - just type in exit

* name - characters only allowed

"""
import json
from api.get_methods import get_unit
from db.database_handler import DatabaseHandler
def main():
    try:
        database = DatabaseHandler()
    except:
        print("Couldn't establish database connection")
        return

    if database.table_exists('db_unit') is False:
        try:
            database.create_table()
        except:
            print("Unable to create database table")
            return

    while True:
        user_input = input("Type in the name of the unit (type exit to exit): ")
        if user_input == "exit":
            database.close_connection()
            print("bye bye")
            break
        if user_input.isdigit():
            print("can't use digits why don't you try characters instead")
        else:
            unit=database.find_unit(user_input)

            if len(unit) > 0:
                unit=json.loads(unit[0][0])
                for key in unit:
                    print(key, ":", unit[key] )
            else:
                unit=get_unit(user_input)
                if len(unit) is 0:
                    print("couldn't find the unit you're looking for")
                else:
                    database.insert_unit(unit)
                    for key in unit:
                        print(key, ":", unit[key])
            print("------------------------")


if __name__ == "__main__":
    main()