'''
This script requires that MySQL Connector and dotenv package be installed within the Python
environment you are running this script in.
'''

import mysql.connector
import os
import json
from dotenv import load_dotenv

class DatabaseHandler:
    """
    A class used to represent database operations

    ...

    Attributes
    ----------
    db_connection : MySQLConnection object
        an object that can be used to create cursor

    Methods
    -------
    * establish_connection()
        establish database connection
    * create_table()
        create database table named db_unit
    * find_unit(user_input)
        select unit that matches user_input from table db_unit
    * insert_unit(unit_to_insert)
        inserts unit returned by api to table db_unit
    *table_exists(table_name)
        check if table exists in database

    """

    def __init__(self):
        '''
        calls establish_connection to establish a database connection
        '''
        load_dotenv()
        self.db_connection= self.establish_connection()

    def establish_connection(self):
        '''
        Returns:
            connection : MySQLConnection object
        Raises:
             Exception : couldn't establish database connection
        '''
        connection = mysql.connector.connect(host=os.getenv("HOST"),
                                             database=os.getenv("DATABASE"),
                                             port = 3306,
                                             user=os.getenv("DB_USER"),
                                             password=os.getenv("DB_PASSWORD"))
        if connection.is_connected():
            return connection
        else:
            raise Exception("connection failed")

    def create_table(self):
        '''Creates database table named db_unit
        '''
        db_cursor = self.db_connection.cursor()
        db_cursor.execute("CREATE TABLE db_unit(unit_id INT NOT NULL,"
                         "unit_name VARCHAR(255) NOT NULL,unit_object JSON NOT NULL)")
        db_cursor.close()

    def find_unit(self,user_input):
        ''' Gets unit by it's name

        Args:
            user_input : str

        Returns:
            unit : list
            the response if unit was found in database
            empty_list : list
            if the input was not str
        '''
        if type(user_input) is not str:
            return {}
        select_sql="select unit_object from db_unit where unit_name='{}'".format(user_input)
        db_cursor = self.db_connection.cursor()
        db_cursor.execute(select_sql)
        unit = db_cursor.fetchall()
        db_cursor.close()
        return unit

    def insert_unit(self,unit_to_insert):
        ''' Store unit in database

            Args:
                unit_to_insert : dict
            Exceptions:
                only dictionary allowed : if anything other than dictionary was passed
        '''
        if type(unit_to_insert) is not dict:
            raise Exception('only dictionary allowed')

        unit_json=json.dumps(unit_to_insert)
        insert_sql="insert into db_unit(unit_id,unit_name,unit_object) values ({},'{}','{}')".format(
                                                                        unit_to_insert['id'],
                                                                        unit_to_insert['name'],unit_json)
        db_cursor = self.db_connection.cursor()
        db_cursor.execute(insert_sql)
        self.db_connection.commit()
        db_cursor.close()

    def table_exists(self,table_name):
        ''' Check if table exists in database

            Args:
                table_name : str

            Returns:
                True: if table exists
                False: if couldn't find table
        '''
        db_cursor=self.db_connection.cursor()
        db_cursor.execute("SELECT COUNT(*) FROM information_schema.tables WHERE "
                          "table_schema='{}' and table_name = '{}'".format(os.getenv("DATABASE"),table_name))

        if db_cursor.fetchone()[0] == 1:
            db_cursor.close()
            return True
        db_cursor.close()
        return False

    def close_connection(self):
        ''' Terminate database connection
        '''
        self.db_connection.close()